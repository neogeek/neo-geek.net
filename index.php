<?php

require('scripts/overseerframework/framework.php');
require('scripts/deblog/deblog.php');

runtime();

$blog = new deBlog;

if ($blog->dir == 'contact') {
	
	if (check_referer('/contact/') && isset($_POST['name'], $_POST['email'], $_POST['message']) && trim($_POST['name']) && trim($_POST['email']) && trim($_POST['message'])) {
		
		if (mail('Neo Geek <neogeek0101@gmail.com>', 'Message from ' . $_POST['name'], $_POST['message'] . str_repeat(PHP_EOL, 2) . ' - ' . $_POST['name'], 'From: ' . $_POST['name'] . ' <' . $_POST['email'] . '>')) {
			
			header('Location: /contact/?success'); exit;
			
		}
		
	}
	
	if (!isset($_GET['success'])) {
		
		$blog->dom->remove($blog->dom->getElementById('msg_success'));
		
	}
	
	if (!count($_POST)) {
		
		$blog->dom->remove($blog->dom->getElementById('msg_error'));
		
	} else {
		
		$blog->dom->getElementById('name')->setAttribute('value', $_POST['name']);
		$blog->dom->getElementById('email')->setAttribute('value', $_POST['email']);
		$blog->dom->getElementById('message')->appendChild($blog->dom->createTextNode($_POST['message']));
		
	}
	
}

$blog->dom->getElementsByTagName('body')->item(0)->appendChild($blog->dom->import('<!-- Page generated in ' . runtime(2) . ' milliseconds. -->'));

?>